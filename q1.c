/* Yasith Samaradivakara
   Index No: 19020742*/


#include <stdio.h>

void pattern(int N);
void pattern(int N){

    if (N >0) {
            pattern(N-1);
            while(N>0){
                printf("%d",N);
                N=N-1;
            }
            printf("\n");

    }

}

int main()
{
    int N;

    printf("Enter N: ");
    scanf("%d", &N);

    pattern(N);

    return 0;
}
